module.exports = function (mongoose, _, workstationModel){
	// Construct schema for workstation collection
	var TaxonomySchema = new mongoose.Schema({
		oslang: [{
			value: String,
		}],
		officelang: [{
			value: String,
		}],
		pacs: [{
			value: String,
		}],
		speechmagic: [{
			value: String,
		}],
		dbserver: [{
			value: String,
		}],
		rislang: [{
			value: String,
		}],
		teams: [{
			value: String,
		}]
	});
	
	var Taxonomy = mongoose.model('Taxonomy', TaxonomySchema);
	var self = this;

	self.get = function (node, callback) {
		var projection = {};
		projection[node] = 1;
		projection['_id'] = 0;
		Taxonomy.find({}, projection, function(err, docs){
			if(err instanceof Error){
				return callback(err);
			} else {
				//console.log(docs[0]);
				return callback(docs[0]);
			}
		});
	}

	self.list = function (callback) {
		Taxonomy.find({}, function(err, docs){
			if(err instanceof Error){
				return callback(err);
			} else {
				return callback(docs);
			}
		});
	}


	self.create = function (node, data, callback) {
		var projection = {};
		projection[node] = data;
		Taxonomy.update({}, {$push : projection}, {upsert: true}, function(err, num, raw){
			if(err instanceof Error){
				console.log(err);
				return callback(err);
			} else {
				//var last = _.last(doc[node]);
				return callback(data);
			}
		});
	}

	self.edit = function (node, _value , data, callback){
		var projection = {};
		projection[node+'.$.value'] = data.value;

		var proj = {};
		proj[node] = {$elemMatch : {value: _value}};
		
		Taxonomy.update(proj, {$set : projection}, {}, function(err, num, raw){
			if(err instanceof Error){
				return callback(err);
			} else {
				workstationModel.updateTaxonomies(node, _value, data.value, function(_err, _numRow, _raw){
					if(_err instanceof Error){
						return callback(_err);
					} else {
						return callback(_numRow);
					}
				});
			}
		});		
	}

	self.delete = function (node, _val, callback) {
		var projection = {}
		projection[node] = {value: _val};	
		workstationModel.checkTaxonomyInUse(node, _val, function(result){
			if(result instanceof Error){
				return callback(err);
			} else {
				if(result == null){
					Taxonomy.update({}, {$pull : projection}, function(err, numberAffected, raw){
						if(err instanceof Error){
							return callback(err);
						} else {
							return callback(numberAffected);
						}
					});
				} else {
					return callback({exists: true});
				}
			}			
		});
	
	}
}