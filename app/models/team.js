module.exports = function (mongoose){
	// Construct schema for workstation collection
	var TeamSchema = new mongoose.Schema({
		name: String, 
		is_active: {type: Boolean, default: true}
	});
	
	var Team = mongoose.model('Team', TeamSchema);
	var self = this;


	self.get = function (callback) {
		Team.find({is_active: true}, function(err, docs){
			if(err instanceof Error){
				return callback(err);
			} else {
				return callback(docs);
			}
		});
	}

	self.create = function (data, callback) {
		var tester = new Team();
		tester.name = data.name;
		tester.save(function(err, doc){
			if(err instanceof Error){
				return callback(err);
			} else {
				return callback(doc);
			}			
		})
	}

	self.edit = function (id, data, callback){
		Team.findByIdAndUpdate(id, data, function(err, doc){
			if(err instanceof Error){
				return callback(err);
			} else {
				return callback(doc);
			}
		});		
	}

	self.delete = function (id, callback) {
		Team.findByIdAndUpdate(id, {is_active: false}, function(err, data){
			if(err instanceof Error){
				return callback(err);
			} else {
				return callback(data);
			}
		});		
	}
}