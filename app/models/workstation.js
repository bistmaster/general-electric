module.exports = function (mongoose, testerModel){
	// Construct schema for workstation collection
	var WorkstationSchema = new mongoose.Schema({
		category: String, 
		type: String, 
		owner_type: String, 
		hostname: String, 
		usage: String, 
		teams: String,
		status: String,
		oslang: String,
		officelang: String,
		pacs: String,
		speechmagic: String, 
		dbserver: String,
		rislang: String, 
		tester: mongoose.Schema.ObjectId,
		comments: String,
		is_active: {type: Boolean, default: true}
	});
	
	var Workstation = mongoose.model('Workstation', WorkstationSchema);
	var self = this;


	self.get = function (callback) {
		Workstation.find({is_active: true}, function(err, docs){
			if(err instanceof Error){
				return callback(err);
			} else {
				return callback(docs);
			}
		});
	}
	
	self.findOne = function (id, callback){
		Workstation.findById(id, function(err, doc){
			if(err instanceof Error){
				return callback(err);
			} else {
				console.log('Tester ' + doc);
				return callback(doc);
			}
		});		
	}

	self.create = function (data, callback) {
		var workstation = new Workstation();
		workstation.category = data.category;
		workstation.type = data.type;
		workstation.owner_type = data.owner_type;
		workstation.hostname = data.hostname;
		workstation.usage = data.usage;
		workstation.teams = data.teams;
		workstation.status = data.status;
		workstation.oslang = data.oslang;
		workstation.officelang = data.officelang;
		workstation.pacs = data.pacs;
		workstation.speechmagic = data.speechmagic;
		workstation.dbserver = data.dbserver;
		workstation.rislang = data.rislang;
		workstation.tester = data.tester;
		workstation.comments = data.comments;
		workstation.save(function(err, doc){
			if(err instanceof Error){
				return callback(err);
			} else {
				return callback(doc);
			}			
		})
	}

	self.edit = function (id, data, callback){
		Workstation.findByIdAndUpdate(id, data, function(err, doc){
			if(err instanceof Error){
				return callback(err);
			} else {
				return callback(doc);
			}
		});		
	}

	self.delete = function (id, callback) {
		Workstation.findByIdAndUpdate(id, {is_active: false}, function(err, data){
			if(err instanceof Error){
				return callback(err);
			} else {
				return callback(data);
			}
		});		
	}

	self.updateTaxonomies = function (node, value, newValue, callback) {
		var query = {};
		query[node] = value;

		var update = {};
		update[node] = newValue;
		Workstation.update(query, update, {multi: true}, function(err, numRow, raw){
			if(err instanceof Error){
				return callback(err);
			} else {
				return callback(raw);
			}
		});
	}

	self.checkTaxonomyInUse = function (node, value, callback){
		var query = {};
		query[node] = value;
		query['is_active'] = true;
		Workstation.findOne(query, function(err, doc){
			if(err instanceof Error){
				return callback(err);
			} else {
				return callback(doc);
			}			
		});
	}
}