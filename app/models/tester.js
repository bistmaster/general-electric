module.exports = function (mongoose){
	// Construct schema for workstation collection
	var TesterSchema = new mongoose.Schema({
		firstname: String, 
		lastname: String, 
		initial: {type: String, uppercase: true},
		is_active: {type: Boolean, default: true}
	});
	
	var Tester = mongoose.model('Tester', TesterSchema);
	var self = this;


	self.get = function (callback) {
		Tester.find({is_active: true}, function(err, docs){
			if(err instanceof Error){
				return callback(err);
			} else {
				return callback(docs);
			}
		});
	}

	self.create = function (data, callback) {
		var tester = new Tester();
		tester.firstname = data.firstname;
		tester.lastname = data.lastname;
		tester.initial = data.initial.toUpperCase();
		tester.save(function(err, doc){
			if(err instanceof Error){
				return callback(err);
			} else {
				return callback(doc);
			}			
		})
	}

	self.edit = function (id, data, callback){
		Tester.findByIdAndUpdate(id, data, function(err, doc){
			if(err instanceof Error){
				return callback(err);
			} else {
				return callback(doc);
			}
		});		
	}

	self.findOne = function (id, callback){
		Tester.findById(id, function(err, doc){
			if(err instanceof Error){
				return callback(err);
			} else {
				console.log('Tester ' + doc);
				return callback(doc);
			}
		});		
	}

	self.delete = function (id, callback) {
		Tester.findByIdAndUpdate(id, {is_active: false}, function(err, data){
			if(err instanceof Error){
				return callback(err);
			} else {
				return callback(data);
			}
		});		
	}
}