var express = require('express'),
	http = require('http');
	mongoose = require('mongoose'),
	util = require('util'),
	_ = require('underscore'),
	socket = require('socket.io'),
	app = express();
	/*Service = require('node-windows').Service;

// Create a new service object
var svc = new Service({
	name:'Workstation Planning',
	description: 'Workstation Planning',
	script: 'C:\\Users\\212350348\\Downloads\\code\\app\\index.js'
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install',function(){
	svc.start();
});
svc.install();*/
 
var CONFIG = {  
	PORT:  process.env.PORT || 8080,
	DB_HOST : 'mongodb://127.0.0.1/workstation',
};	
	
app.configure('development',function () {

	app.set('port', CONFIG.PORT) 
	app.set('view engine', 'ejs'); 
	app.set('views', __dirname + '/views');
	app.use(express.logger('dev'));
	app.use(express.cookieParser());		
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(app.router);	
	app.use(express.static(__dirname + '/public'));
	app.use(express.session({ secret: 'general-electric' }));
});		

// for product configuration of the middleware
app.configure('production', function(){
	app.set('port', CONFIG.PORT); 
	app.set('view engine', 'ejs'); 
	app.set('views', __dirname + '/views');
	app.use(express.bodyParser());
	app.use(express.cookieParser());		
	app.use(express.methodOverride());
	app.use(app.router);	
	
	app.use(express.static(__dirname + '/public'));
	app.use(express.session({ secret: 'general-electric' }));		
});	

// connect to mongodb
mongoose.connect(CONFIG.DB_HOST, function onMongooseError(err){
	if (err instanceof Error) throw err;
		console.log('Connected to db');
}); 

var routers = require('./routers/index')(app, mongoose, _);

// create an http server to listen and passt the express
http.createServer(app).listen(app.get('port'), function onPortListen(){
	console.log('Server is up and listening to ' + app.get('port'));
}); 