controllers.WorkstationCtrl = function($scope, WorkstationFactory){
	$scope.loading = true;
	WorkstationFactory.read(function(result){
		$scope.workstations = result.data;
		angular.forEach($scope.workstations, function(value, key){
			WorkstationFactory.getTester(value.tester, function(result){
				if(result.data){
					value.tester = result.data.firstname  + ' - ' + result.data.lastname;
				} else {
					value.tester = 'No Tester';
				}
			});
		});	
		$scope.loading = false;
	});
}

controllers.WorkstationCreateCtrl = function($scope, WorkstationFactory, $location, CommonFactory){
	// fetch all the testers
	$scope.saveBtn = "Save";
	$scope.isCreate = true;
	WorkstationFactory.readTester(function(result){
		if(result.success){
			$scope.testers = result.data;
		} else {
			console.log('Unable to read tester');
		}
	});	

	CommonFactory.getAllTaxonomy(function(result){
		if(result.success){
			try{
				$scope.teams = result.data[0].teams;
				$scope.oslangs = result.data[0].oslang;
				$scope.officelangs = result.data[0].officelang;
				$scope.pacs = result.data[0].pacs;
				$scope.speechmagics = result.data[0].speechmagic;
				$scope.dbservers = result.data[0].dbserver;
				$scope.rislangs = result.data[0].rislang;
			} catch(err){}
		}
	});


	// adding a workstation
	$scope.saveWorkstation = function () {
		WorkstationFactory.create($scope.model, function (result){
			if(result.success){
				WorkstationFactory.addWorkstation($scope.model);
				$location.path('/');
			} else {
				console.log('Unable to save workstation');
			}
		});
	}
}

controllers.WorkstationEditCtrl = function($scope, WorkstationFactory, CommonFactory, $routeParams, _, $location, $modal){
	// fetch all the testers
	var id = $routeParams.workstation_id;
	var path = $location.path().split('/')[2];
	$scope.readonly = ('details' == path) ? true : false;
	$scope.saveBtn = "Save Changes";

	CommonFactory.getAllTaxonomy(function(result){
		if(result.success){
			$scope.teams = result.data[0].teams;
			$scope.oslangs = result.data[0].oslang;
			$scope.officelangs = result.data[0].officelang;
			$scope.pacs = result.data[0].pacs;
			$scope.speechmagics = result.data[0].speechmagic;
			$scope.dbservers = result.data[0].dbserver;
			$scope.rislangs = result.data[0].rislang;
		}
	});

	WorkstationFactory.readTester(function(result){
		if(result.success){
			$scope.testers = result.data;
		} else {
			console.log('Unable to read tester');
		}
	});

	// fetch all the teams
	WorkstationFactory.readWorkstation(id, function(result){
		if(result.success){
			try{
				$scope.model = result.data;
				$scope.model.rislang = _.findWhere($scope.rislangs, { value :result.data.rislang}).value;
				$scope.model.dbserver = _.findWhere($scope.dbservers, { value :result.data.dbserver}).value;
				$scope.model.speechmagic = _.findWhere($scope.speechmagics, { value :result.data.speechmagic}).value;
				$scope.model.pacs = _.findWhere($scope.pacs, { value :result.data.pacs}).value;
				$scope.model.officelang = _.findWhere($scope.officelangs, { value :result.data.officelang}).value;
				$scope.model.oslang = _.findWhere($scope.oslangs, { value :result.data.oslang}).value;
				$scope.model.location = _.findWhere($scope.teams, { value :result.data.location}).value;
			} catch(err) {}
		} else {
			console.log('Unable to read workstation');
		}
	});

	// adding a workstation
	$scope.saveWorkstation = function () {
		var data = $scope.model;
		delete data['_id'];
		console.log(data);
		WorkstationFactory.update(id, data, function (result){
			if(result.success){
				//WorkstationFactory.addWorkstation($scope.model);
				$location.path('/');
			} else {
				console.log('Unable to save workstation');
			}
		});
	}

	$scope.deleteWorkstation = function() {
		var modalInstance = $modal.open({
			templateUrl: '/js/partials/workstation/confirmation.html',
			controller: controllers.ConfirmationModalInstanceCtrl,
			resolve: { 
				data: function() {
					return {	
								title: 'Workstation', 
								message:'Are you sure you want to delete this Workstation?',
								id: id,
								type: 'workstation',
								index: null,
								data:[],
								disabled_delete: false
							};
				}
			}
		});		
	}
}


controllers.WorkstationTesterCtrl = function($scope, WorkstationFactory, _, $modal){
	$scope.editMode = false;
	$scope.duplicateInitial = false;
	$scope.duplicateEditInitial = false;
	WorkstationFactory.readTester(function(result){
		if(result.success && result.data){
			_.each(result.data, function(data) { data.editMode = false; });			
			$scope.testers = result.data;
			WorkstationFactory.setTesters = result.data;
		} else {
			console.log('Unable to read tester');
		}
	});

	// saves tester
	$scope.saveTester = function() {
		WorkstationFactory.createTester($scope.tester, function(result){
			if(result.success){
				$scope.testers.push(result.data);
				WorkstationFactory.addTester(result.data);
			}  else {
				console.log('Unable to save tester');
			}
			$scope.clearTester();
		});
	}

	$scope.checkInitial = function (mode) {
		try {
			if(mode) {
				var initialEdit = _.findWhere($scope.testers, { initial : $scope.edit.initial.toUpperCase()});
				if(initialEdit){
					$scope.duplicateEditInitial = true;
				} else {
					$scope.duplicateEditInitial = false;
				}				
			} else {
				var initial = _.findWhere($scope.testers, { initial : $scope.tester.initial.toUpperCase()});
				if(initial){
					$scope.duplicateInitial = true;
				} else {
					$scope.duplicateInitial = false;
				}
			}	
			
		} catch(err){}
	}

	$scope.clearTester = function () {
		$scope.tester.firstname = '';
		$scope.tester.lastname = '';
		$scope.tester.initial = '';
	}

	$scope.deleteTester = function(id, index){
		WorkstationFactory.deleteTester(id, function (result) {
			if(result.success){
				$scope.testers .splice(index, 1);
			} else if(result.exists){
				var modalInstance = $modal.open({
					templateUrl: '/js/partials/workstation/confirmation.html',
					controller: controllers.ConfirmationModalInstanceCtrl,
					resolve: { 
						data: function() {
							return {	
										title: 'Tester', 
										message:'Cannot delete this tester, It\'s currently in used.',
										id: id,
										type: 'tester',
										index: null,
										data: null,
										disabled_delete: true
									};
						}
					}
				});			
			}
		});	
	}

	$scope.edit = function (index, tester) {
		_.each($scope.testers, function(tester){
			tester.editMode = false;
		});		
		
		$scope.testers[index].editMode = true;
		$scope.edit.firstname = tester.firstname;
		$scope.edit.lastname = tester.lastname;
		$scope.edit.initial = tester.initial;
	}

	$scope.update = function (index, id) {
		WorkstationFactory.editTester(id, {firstname: $scope.edit.firstname, lastname: $scope.edit.lastname, initial: $scope.edit.initial}, function (result){
			if(result.success){
				$scope.testers[index].firstname = $scope.edit.firstname;
				$scope.testers[index].lastname = $scope.edit.lastname;
				$scope.testers[index].initial = $scope.edit.initial.toUpperCase();
				$scope.testers[index].editMode = false;
			}
		});
	}

	$scope.cancel = function (index) {
		$scope.testers[index].editMode = false;
	}	
}


controllers.CommonCtrl = function ($scope, $location, CommonFactory, _ , $modal){
	var path = $location.path().split('/')[2];
	$scope.taxonomies = [];
	$scope.editMode = false;

	switch(path.toLowerCase()){
		case 'oslang' : $scope.headerName = 'Os and Language'; break;
		case 'officelang' : $scope.headerName = 'Microsoft Office and Language'; break;
		case 'pacs' : $scope.headerName = 'PACS'; break;
		case 'speechmagic' : $scope.headerName = 'Speechmagic'; break;
		case 'dbserver' : $scope.headerName = 'DB Server'; break;
		case 'rislang' : $scope.headerName = 'RIS-i Language'; break;
		case 'teams' : $scope.headerName = 'Teams'; break;
	}

	CommonFactory.getTaxonomy(path, function(result){
		if(result.success && result.data){
			var datas = _.values(result.data)[0];
			_.each(datas, function(data) { data.editMode = false; });
			$scope.taxonomies = datas;
		} else {
			$scope.taxonomies = [];
		}
	});

	// trigger when save is being clicked
	$scope.save = function () {
		if($scope.model.value.length != 0){
			CommonFactory.create(path, $scope.model, function (result){
				if(result.success){
					result.data.editMode = false;
					$scope.taxonomies.push(result.data);
					$scope.model.value = '';
				}
			});
		}
	}
	// deletes a specific taxonomy
	$scope.delete = function(value, idx){
		CommonFactory.deleteTaxonomy(path, value, function (result){
			if(result.success && result.data == 1){
				$scope.taxonomies.splice(idx, 1);
			} else {
				var modalInstance = $modal.open({
				templateUrl: '/js/partials/workstation/confirmation.html',
				controller: controllers.ConfirmationModalInstanceCtrl,
				resolve: { 
						data: function() {
							return {	
										title: 'Confirmation', 
										message:'Cannot delete ' + value +', It\'s currently in used.',
										id: null,
										type: 'taxonomy',
										index: null,
										data: null,
										disabled_delete: true,
									};
						}
					}
				});					
			}			
		});
	}

	// clear the textfield
	$scope.clear = function (){
		try{
			$scope.model.value = "";
		} catch(err){

		}
	}

	// edit the specific taxonomy
	$scope.edit = function(index, value) {
		_.each($scope.taxonomies, function(taxonomy){
			taxonomy.editMode = false;
		});

		$scope.taxonomies[index].editMode = true;
		$scope.edit.value = value;
	}

	//update specific taxonomy 
	$scope.update = function (index, value){
		CommonFactory.updateTaxonomy(path, value, {value: $scope.edit.value}, function (result) {
			if(result.success){
				$scope.taxonomies[index].value = $scope.edit.value;
				$scope.taxonomies[index].editMode = false;				
			}
		});
	}

	$scope.cancel = function(index) {
		$scope.taxonomies[index].editMode = false;
	}

	$scope.ignoreSpecialCharacters = function (editMode) {
		if(editMode){
			if($scope.edit.value){
				$scope.edit.value = $scope.edit.value.replace(/[^\w\s]/gi, '');
			}
		} else {
			if($scope.model.value){
				$scope.model.value = $scope.model.value.replace(/[^\w\s]/gi, '');
			}
		}
	}
}

controllers.ConfirmationModalInstanceCtrl = function($scope, $modalInstance, WorkstationFactory, data, $location){
	$scope.model = {};
	$scope.model.title = data.title; 
	$scope.model.message = data.message; 
	$scope.disableDelete = data.disabled_delete;
	$scope.delete = function () {
		if('workstation' == data.type) {
			WorkstationFactory.delete(data.id, function (result) {
				if(result.success){
					$location.path('/');
				}
			});
		} else if('tester' == data.type){
			
		} else if('taxonomy' == data.type){

		}
		$modalInstance.dismiss('cancel');
	};

	$scope.close = function () {
		$modalInstance.dismiss('cancel');
	};
}

controllers.TestcaseCtrl = function($scope){

}

geApp.controller(controllers);