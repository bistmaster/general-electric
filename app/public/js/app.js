var geApp = angular.module('geApp', ['ui.bootstrap']);
var controllers = {};

geApp.config(function($routeProvider){
	$routeProvider
		.when('/', { templateUrl: '/js/partials/workstation/workstation.html', controller: 'WorkstationCtrl'})
		.when('/workstation', { templateUrl: '/js/partials/workstation/workstation.html', controller: 'WorkstationCtrl'})
		.when('/workstation/add', { templateUrl: '/js/partials/workstation/workstation-add.html', controller: 'WorkstationCreateCtrl'})
		.when('/workstation/edit/:workstation_id', { templateUrl: '/js/partials/workstation/workstation-add.html', controller: 'WorkstationEditCtrl'})
		.when('/workstation/details/:workstation_id', { templateUrl: '/js/partials/workstation/workstation-add.html', controller: 'WorkstationEditCtrl'})
		.when('/workstation/tester', { templateUrl: '/js/partials/workstation/workstation-tester.html', controller: 'WorkstationTesterCtrl'})
		.when('/workstation/teams', { templateUrl: '/js/partials/workstation/workstation-taxonomy.html', controller: 'CommonCtrl'})
		.when('/workstation/oslang', { templateUrl: '/js/partials/workstation/workstation-taxonomy.html', controller: 'CommonCtrl'})
		.when('/workstation/officelang', { templateUrl: '/js/partials/workstation/workstation-taxonomy.html', controller: 'CommonCtrl'})
		.when('/workstation/pacs', { templateUrl: '/js/partials/workstation/workstation-taxonomy.html', controller: 'CommonCtrl'})
		.when('/workstation/speechmagic', { templateUrl: '/js/partials/workstation/workstation-taxonomy.html', controller: 'CommonCtrl'})
		.when('/workstation/dbserver', { templateUrl: '/js/partials/workstation/workstation-taxonomy.html', controller: 'CommonCtrl'})
		.when('/workstation/rislang', { templateUrl: '/js/partials/workstation/workstation-taxonomy.html', controller: 'CommonCtrl'})
		.when('/testcase', { templateUrl: '/js/partials/planning.html', controller: 'TestcaseCtrl'});
});
