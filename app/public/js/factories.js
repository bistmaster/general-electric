geApp.factory('WorkstationFactory', function ($http, $rootScope) {

	var WorkstationFactory = {};
	WorkstationFactory.data = {};
	WorkstationFactory.testers = [];
	WorkstationFactory.teams = [];
	WorkstationFactory.workstations = [];
	WorkstationFactory.tester = {};

	// Gets all the workstations from the server
	WorkstationFactory.read = function (callback) {
		$http.get('/workstations').success(function(result, status, headers, config){
			return callback(result);
		});
	}

	WorkstationFactory.readWorkstation = function (id, callback) {
		$http.get('/workstation/read/' + id).success(function(result, status, headers, config){
			return callback(result);
		});
	}

	// Create a workstation sending a post request to the server
	WorkstationFactory.create = function (data, callback) {
		$http.post('/workstation/create', data).success(function(result, status, headers, config){
			return callback(result);
		});
	}

	// Create a workstation sending a post request to the server
	WorkstationFactory.update = function (id, data, callback) {
		$http.put('/workstation/update/'+ id, data, { method: 'PUT'}).success(function(result, status, headers, config){
			return callback(result);
		});
	}

	// Create a workstation sending a post request to the server
	WorkstationFactory.delete = function (id, callback) {
		$http.get('/workstation/delete/'+id).success(function(result, status, headers, config){
			return callback(result);
		});
	}

	WorkstationFactory.readTester = function (callback) {
		$http.get('/testers').success(function(result, status, headers, config){
			return callback(result);
		});
	}

	// Create a workstation sending a post request to the server
	WorkstationFactory.createTester = function (data, callback) {
		$http.post('/tester/create', data).success(function(result, status, headers, config){
			return callback(result)
		});
	}

	// Create a workstation sending a post request to the server
	WorkstationFactory.editTester = function (id, data, callback) {
		$http.put('/tester/update/' + id, data, { method: 'PUT'}).success(function(result, status, headers, config){
			return callback(result);
		});
	}

	// Create a workstation sending a post request to the server
	WorkstationFactory.deleteTester = function (id, callback) {
		$http.get('/tester/delete/' + id).success(function(result, status, headers, config){
			return callback(result);
		});
	}

	// Read tester
	WorkstationFactory.getTester = function (id, callback) {
		$http.get('/tester/read/'+id).success(function(result, status, headers, config){
			return callback(result);
		});
	}

	WorkstationFactory.setTesters = function (testers) {
		this.testers = testers;
	}	

	WorkstationFactory.setTeams = function (teams) {
		this.teams = teams;
	}

	WorkstationFactory.addWorkstation = function (workstation) {
		this.workstations.push(workstation);
	}	

	WorkstationFactory.addTester = function (tester) {
		this.testers.push(tester);
	}	

	WorkstationFactory.addTeam = function (team) {
		this.teams.push(team);
	}

	return WorkstationFactory;
});

geApp.factory('CommonFactory', function ($http, $rootScope){
	var CommonFactory = {};

	// Create a workstation sending a post request to the server
	CommonFactory.create = function (node, data, callback) {
		$http.post('/taxonomy/create/' + node, data).success(function(result, status, headers, config){
			return callback(result);
		});
	}

	CommonFactory.getTaxonomy = function (node, callback) {
		$http.get('/taxonomy/'+ node).success(function(result, status, headers, config){
			return callback(result);
		});
	}

	CommonFactory.getAllTaxonomy = function (callback) {
		$http.get('/taxonomy').success(function(result, status, headers, config){
			return callback(result);
		});
	}

	CommonFactory.deleteTaxonomy = function (node, value, callback) {
		$http.get('/taxonomy/delete/'+ node + '/'+value).success(function(result, status, headers, config){
			return callback(result);
		});
	}

	CommonFactory.updateTaxonomy = function (node, value, data, callback) {
		$http.put('/taxonomy/update/'+ node + '/'+value, data, {method : 'PUT'}).success(function(result, status, headers, config){
			return callback(result);
		});
	}	

	return CommonFactory;
});

// create a underscore utility factory
geApp.factory('_', function() {
	return window._;
});
