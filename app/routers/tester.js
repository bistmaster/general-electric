/**
 * Workstation routers 	Manages the incoming request of the workstation
 * @param workstationModel In-charge on manipulating the workstation to the database
 */
module.exports = function (testerModel){

	var self = this;
	
	// handles on creating a product
	self.create = function (req, res){
		console.log('tester ' + req.body);
		testerModel.create(req.body, function(result){
			if(result instanceof Error){
				res.json({success: false, data: result});		 
			} else {
				res.json({success: true, data: result});
			}
		});		
	};

	// handles on deleting a product by id
	self.delete = function(req, res){
		var id = req.params.id;
		testerModel.delete(id, function(result){
			if(result instanceof Error){
				res.json({success: false});		
			} else {
				res.json({success: true, data: result});
			}
		});
	};


	// handles on updating the product by id
	self.update = function(req, res){
		var id = req.params.id;
		req.body.initial = req.body.initial.toUpperCase();
		testerModel.edit(id, req.body, function(result){
			if(result instanceof Error){
				res.json({success: false});		
			} else {
				res.json({success: true, data: result});
			}
		});
	};

	// handles on updating the product by id
	self.read = function(req, res){
		var id = req.params.id;
		testerModel.findOne(id, function(result){
			if(result instanceof Error){
				res.json({success: false, data: result});		
			} else {
				res.json({success: true, data: result});
			}
		});
	};

	// handles on fetching the list of products
	self.list = function (req, res){
		testerModel.get(function(result){
			if(result instanceof Error){
				res.json({success: false, data: result});		
			} else {
				res.send({success: true, data: result});
			}
		});
	};
}