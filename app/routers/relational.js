/**
 * Workstation routers 	Manages the incoming request of the workstation
 * @param workstationModel In-charge on manipulating the workstation to the database
 */
module.exports = function (workstationModel, testerModel){

	var self = this;
	
	// handles on creating a product
	self.deleteTester = function (req, res){
		var id = req.params.id;
		workstationModel.checkTaxonomyInUse('tester', id, function(result){
			if(result instanceof Error){
				return callback(err);
			} else {
				if(null == result){
					testerModel.delete(id, function(result){
						if(result instanceof Error){
							res.json({success: false});		
						} else {
							res.json({success: true, data: result});
						}
					});						
				} else {
					res.json({exists: true});
				}

			}		
		}); 

	};

}