module.exports = function(app, mongoose, _){

	// instatiate routes
	var WorkstationRoutes = require('./workstation'),
		TesterRoutes = require('./tester'),
		TaxonomyRoutes = require('./taxonomy'),
		RelationalRoutes = require('./relational');

	var	WorkstationModel = require('./../models/workstation'),
		TesterModel = require('./../models/tester'),
		TaxonomyModel = require('./../models/taxonomy');


	// instantiate model	
	var	testerModel = new TesterModel(mongoose),
		workModel = new WorkstationModel(mongoose, testerModel),
		taxonomyModel = new TaxonomyModel(mongoose, _, workModel);

	var	testerRoutes = new TesterRoutes(testerModel),
		workRoutes = new WorkstationRoutes(workModel),
		taxonomyRoutes = new TaxonomyRoutes(taxonomyModel),
		relationalRoutes = new RelationalRoutes(workModel, testerModel);



	app.get('/', function(req, res){
		res.render('index');
	});

	// Workstation routes
	app.post('/workstation/create', workRoutes.create);
	app.put('/workstation/update/:id', workRoutes.update);
	app.get('/workstation/read/:id', workRoutes.read);
	app.get('/workstation/delete/:id', workRoutes.delete);
	app.get('/workstations', workRoutes.list);

	// Tester routes
	app.post('/tester/create', testerRoutes.create);
	app.put('/tester/update/:id', testerRoutes.update);
	app.get('/tester/delete/:id', relationalRoutes.deleteTester);
	app.get('/tester/read/:id', testerRoutes.read);
	app.get('/testers', testerRoutes.list);	

	// Taxonomy routes
	app.post('/taxonomy/create/:node', taxonomyRoutes.create);
	app.put('/taxonomy/update/:node/:value', taxonomyRoutes.update);
	app.get('/taxonomy/delete/:node/:value', taxonomyRoutes.delete);
	app.get('/taxonomy/:node', taxonomyRoutes.list);		
	app.get('/taxonomy', taxonomyRoutes.AllTaxonomies);		

}