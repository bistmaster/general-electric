/**
 * Workstation routers 	Manages the incoming request of the workstation
 * @param workstationModel In-charge on manipulating the workstation to the database
 */
module.exports = function (teamModel){

	var self = this;
	
	// handles on creating a product
	self.create = function (req, res){
		//console.log(req.body);
		teamModel.create(req.body, function(result){
			if(result instanceof Error){
				res.json({success: false, data: result});		 
			} else {
				res.json({success: true, data: result});
			}
		});		
	};

	// handles on deleting a product by id
	self.delete = function(req, res){
		var id = req.params.id;
		teamModel.delete(id, function(result){
			if(result instanceof Error){
				res.json({success: false});		
			} else {
				res.json({success: true, data: result});
			}
		});
	};


	// handles on updating the product by id
	self.update = function(req, res){
		var id = req.params.id;
		teamModel.edit(id, req.body, function(result){
			if(result instanceof Error){
				res.json({success: false});		
			} else {
				res.json({success: true, data: result});
			}
		});
	};

	// handles on fetching the list of products
	self.list = function (req, res){
		teamModel.get(function(result){
			if(result instanceof Error){
				res.json({success: false, data: result});		
			} else {
				res.send({success: true, data: result});
			}
		});
	};
}