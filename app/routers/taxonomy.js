/**
 * Workstation routers 	Manages the incoming request of the workstation
 * @param workstationModel In-charge on manipulating the workstation to the database
 */
module.exports = function (taxonomyModel){

	var self = this;
	
	// handles on creating a product
	self.create = function (req, res){
		var node = req.params.node;
		taxonomyModel.create(node, req.body, function(result){
			if(result instanceof Error){
				res.json({success: false, data: result});		 
			} else {
				res.json({success: true, data: result});
			}
		});		
	};

	// handles on deleting a product by id
	self.delete = function(req, res){ 
		var value = req.params.value,
		 	node = req.params.node;
		taxonomyModel.delete(node, value, function(result){
			if(result instanceof Error){
				res.json({success: false});		
			} else {
				res.json({success: true, data: result});
			}
		});
	};


	// handles on updating the product by id
	self.update = function(req, res){
		var value = req.params.value,
			node = req.params.node;
		taxonomyModel.edit(node, value, req.body, function(result){
			if(result instanceof Error){
				res.json({success: false, data: result});		
			} else {
				res.json({success: true, data: result});
			}
		});
	};

	// handles on fetching the list of products
	self.list = function (req, res){
		var node = req.params.node;
		taxonomyModel.get(node, function(result){
			if(result instanceof Error){
				res.json({success: false, data: result});		
			} else {
				res.send({success: true, data: result});
			}
		});
	};

	self.AllTaxonomies = function (req, res){
		var node = req.params.node;
		taxonomyModel.list(function(result){
			if(result instanceof Error){
				res.json({success: false, data: result});		
			} else {
				res.send({success: true, data: result});
			}
		});
	};	
}