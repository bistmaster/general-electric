/**
 * Workstation routers 	Manages the incoming request of the workstation
 * @param workstationModel In-charge on manipulating the workstation to the database
 */
module.exports = function (workstationModel, testerModel){

	var self = this;
	
	// handles on creating a product
	self.create = function (req, res){
		//console.log(req.body);
		workstationModel.create(req.body, function(result){
			if(result instanceof Error){
				res.json({success: false, data: result});		 
			} else {
				res.json({success: true, data: result});
			}
		});		
	};

	// handles on deleting a product by id
	self.delete = function(req, res){
		var id = req.params.id;
		workstationModel.delete(id, function(result){
			if(result instanceof Error){
				res.json({success: false});		
			} else {
				res.json({success: true, data: result});
			}
		});
	};

	// handles on updating the product by id
	self.read = function(req, res){
		var id = req.params.id;
		workstationModel.findOne(id, function(result){
			if(result instanceof Error){
				res.json({success: false, data: result});		
			} else {
				res.json({success: true, data: result});
			}
		});
	};
	// handles on updating the product by id
	self.update = function(req, res){
		var id = req.params.id;
		console.log('received request ' + id);
		console.log('received request');
		console.log(req.body);
		workstationModel.edit(id, req.body, function(result){
			if(result instanceof Error){
				res.json({success: false, data: result});		
			} else {
				res.json({success: true, data: result});
			}
		});
	};

	// handles on fetching the list of products
	self.list = function (req, res){
		workstationModel.get(function(result){
			if(result instanceof Error){
				res.json({success: false});		
			} else {
				res.send({success: true, data: result});
			}
		});
	};
}